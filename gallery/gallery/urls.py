
from django.contrib.auth.views import logout_then_login
from django.conf.urls import include, url
from django.contrib import admin


from app.views import *
from app.ajax import *


urlpatterns = [
    url(r'^admn/grappelli/', include('grappelli.urls')),
    url(r'^admn/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admn/', include(admin.site.urls)),
    url(r'^$', AppLoginView.as_view(), name="login"),
    url(r'^logout/$', lambda request: logout_then_login(request, "/"), name="logout"),
    url(r'^albums/$', AlbumsPage.as_view(), name='main'),
    url(r'^albums/(?P<album_pk>\d+)/$', AlbumPage.as_view(), name='album'),
    url(r'^albums/upload', UploadPage.as_view(), name='upload'),
    url(r'^albums/setting/(?P<album_pk>\d+)/$', AlbumSettingPage.as_view(), name='album_setting'),
    url(r'^api/albums/create', create_album, name='create_album'),
    url(r'^api/album/cover/(?P<album_pk>\d+)/$', cover_set, name="cover_set"),
    url(r'^api/album/upload/(?P<album_pk>\d+)/$', upload_album, name='upload_album'),
    url(r'^api/photo/upload/(?P<album_pk>\d+)/$', upload_photo, name='upload_photo'),
    url(r'^api/setting/(?P<album_pk>\d+)/$', save_setting, name='save_setting'),
    url(r'^api/photo/delete', delete_photo, name='delete_photo'),
    url(r'^api/user/create', create_user, name='create_user'),
    url(r'^api/user/info/(?P<user_pk>\d+)/$', info_user, name='info_user'),
    url(r'^api/user/save/(?P<user_pk>\d+)/$', save_user, name='save_user'),
    url(r'^api/download/photo/(?P<photo_pk>\d+)/$', download_photo, name='download_photo'),
    url(r'^adm/$', AdminDashboardView.as_view(), name="admin_dashboard"),
]
