# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model, authenticate

from nnmware.core.exceptions import UserIsDisabled

from models import GalleryAlbum


class AppLoginForm(forms.Form):
    username = forms.CharField(label=_('Username'), max_length=30)
    password = forms.CharField(label=_('Password'), max_length=30)

    class Meta:
        widgets = dict(password=forms.PasswordInput)

    def clean_username(self):
        username = self.cleaned_data["username"]
        if not username:
            raise forms.ValidationError(_("Username is required"), code='invalid')
        try:
            user = get_user_model().objects.get(username=username)
            if not user.is_active:
                raise UserIsDisabled
            return username
        except get_user_model().DoesNotExist:
            raise forms.ValidationError(_("Username not exist"), code='invalid')
        except UserIsDisabled:
            raise forms.ValidationError(_("User is disabled"), code='invalid')

    def clean_password(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        if not user:
            raise forms.ValidationError(_("Password is incorrect"), code='invalid')
        return password


