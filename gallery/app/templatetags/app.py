# -*- coding: utf-8 -*-

from django.template import Library

from ..models import GalleryAlbum, GalleryUser, GalleryPhoto

register = Library()


@register.assignment_tag(takes_context=True)
def album_names(context):
    request = context['request']
    if request.user.is_superuser:
        result = GalleryAlbum.objects.all
    else:
        result = GalleryAlbum.objects.filter(users=request.user)
    return result


@register.assignment_tag
def user_albums(user_pk):
    user = GalleryUser.objects.get(pk=user_pk)
    albums = user
    return albums


@register.assignment_tag
def all_users():
    return GalleryUser.objects.all


@register.assignment_tag
def all_albums():
    albums = GalleryAlbum.objects.all
    albms = []
    for i in albums:
        albms.append(i.name)
    return albms


@register.simple_tag
def total_album():
    return GalleryAlbum.objects.count()