# -*- coding: utf-8 -*-

from django.contrib.auth import get_user_model, authenticate, login
from django.views.generic import FormView
from django.views.generic import TemplateView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.list import ListView
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404

from nnmware.core.views import AjaxFormMixin, CurrentUserSuperuser

from forms import AppLoginForm
from models import GalleryAlbum, GalleryPhoto


class AuthCheckMixin(object):
    """check whether the user is logged
    in when you click on the direct url"""

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseRedirect('/')
        return super(AuthCheckMixin, self).dispatch(request, *args, **kwargs)


class AppLoginView(AjaxFormMixin, FormView):
    form_class = AppLoginForm
    success_url = "albums/"
    template_name = 'login.html'

    def form_valid(self, form):
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        login(self.request, user)
        return super(AppLoginView, self).form_valid(form)


class AlbumsPage(AuthCheckMixin, ListView):
    template_name = 'albums.html'
    context_object_name = "albums"

    def get_queryset(self):
        if self.request.user.is_superuser:
            result_list = GalleryAlbum.objects.all
        else:
            result_list = GalleryAlbum.objects.filter(users=self.request.user)
        return result_list


class AlbumPage(AuthCheckMixin, SingleObjectMixin, ListView):
    template_name = 'album.html'

    def get_object(self, queryset=None):
        return get_object_or_404(GalleryAlbum, pk=int(self.kwargs['album_pk']))

    def get_queryset(self):
        self.object = self.get_object()
        return GalleryPhoto.objects.filter(album=self.object)


class UploadPage(AuthCheckMixin, TemplateView):
    template_name = 'upload.html'


class AlbumSettingPage(AuthCheckMixin, SingleObjectMixin, ListView):
    template_name = 'album_setting.html'

    def get_object(self, queryset=None):
        return get_object_or_404(GalleryAlbum, pk=int(self.kwargs['album_pk']))

    def get_queryset(self):
        self.object = self.get_object()
        return GalleryPhoto.objects.filter(album=self.object)

class AdminDashboardView(CurrentUserSuperuser, ListView):
    template_name = 'admin/admin.html'

    def get_queryset(self):
        return get_user_model().objects.all()
